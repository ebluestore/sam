<?php

class Meigee_Layouts_Model_Key extends Varien_Object
{
    static public function toOptionArray()
    {
        return array(
            array('value' => 'home_left_1', 'label'=>Mage::helper('adminhtml')->__('Home Left 1')),
            array('value' => 'home_left_2', 'label'=>Mage::helper('adminhtml')->__('Home Left 2')),
            array('value' => 'home_contenttop_1', 'label'=>Mage::helper('adminhtml')->__('Home Contenttop 1')),
            array('value' => 'home_contenttop_2', 'label'=>Mage::helper('adminhtml')->__('Home Contenttop 2')),
            array('value' => 'home_contenttop_3', 'label'=>Mage::helper('adminhtml')->__('Home Contenttop 3')),
            array('value' => 'home_contentbottom_1', 'label'=>Mage::helper('adminhtml')->__('Home Content Bottom 1')),
            array('value' => 'home_contentbottom_2', 'label'=>Mage::helper('adminhtml')->__('Home Content Bottom 2')),
            array('value' => 'home_today_sales', 'label'=>Mage::helper('adminhtml')->__('Home Today Sales')),
            array('value' => 'home_center_products', 'label'=>Mage::helper('adminhtml')->__('Home Center Products')),
            array('value' => 'home_bottom_products', 'label'=>Mage::helper('adminhtml')->__('Home Bottom Products')),
            array('value' => 'footer_left_block', 'label'=>Mage::helper('adminhtml')->__('Footer Left Block')),
            array('value' => 'footer_center_block_products', 'label'=>Mage::helper('adminhtml')->__('Footer Center Block Products')),
            array('value' => 'footer_center_block_tweets', 'label'=>Mage::helper('adminhtml')->__('Footer Center Block Tweets')),
            array('value' => 'footer_center_block_help', 'label'=>Mage::helper('adminhtml')->__('Footer Center Block Help')),
            array('value' => 'footer_center_block_tags', 'label'=>Mage::helper('adminhtml')->__('Footer Center Block Tags')),
            array('value' => 'footer_right_block', 'label'=>Mage::helper('adminhtml')->__('Footer Right Block')),
        );
    }

    static public function getBlockHtml()
    {
        $html = '
			<table cellpadding="0" cellspacing="0" style="width:500px">
				<thead>
					<tr>
						<th scope="col">Block Key</th>
						<th scope="col">Image</th>
						<th scope="col">Retina Image</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Home Left 1</td>
						<td>400x671</td>
						<td>800x1342</td>
					</tr>
					<tr>
						<td>Home Left 2</td>
						<td>178x82</td>
						<td>356x164</td>
					</tr>
					<tr>
						<td>Home Contenttop 1</td>
						<td>400x387</td>
						<td>800x774</td>
					</tr>
					<tr>
						<td>Home Contenttop 2</td>
						<td>400x387</td>
						<td>800x774</td>
					</tr>
					<tr>
						<td>Home Contenttop 3</td>
						<td>400x387</td>
						<td>800x774</td>
					</tr>
					<tr>
						<td>Home Content Bottom 1</td>
						<td>404x400</td>
						<td>808x800</td>
					</tr>
					<tr>
						<td>Home Content Bottom 2</td>
						<td>510x440</td>
						<td>1020x880</td>
					</tr>
					<tr>
						<td>Home Today Sales</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Home Center Products</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Home Bottom Products</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Footer Left Block</td>
						<td>241x220</td>
						<td>482x440</td>
					</tr>
					<tr>
						<td>Footer Center Block Products</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Footer Center Block Tweets</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Footer Center Block Help</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Footer Center Block Tags</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
					<tr>
						<td>Footer Right Block</td>
						<td>no image</td>
						<td>no image</td>
					</tr>
				</tbody>
			</table>
        ';
        return $html;
    }
}