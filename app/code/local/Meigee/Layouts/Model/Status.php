<?php

class Meigee_Layouts_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 2;

    static public function toOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('layouts')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('layouts')->__('Disabled')
        );
    }
}