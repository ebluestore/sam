<?php

class Meigee_Layouts_Model_Mysql4_Layouts_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('layouts/layouts');
    }

    public function addStoreFilter($storeIds = array(), $withDefaultStore = true)
    {
        if (!is_array($storeIds)) {
            $storeIds = array($storeIds);
        }
        if ($withDefaultStore && !in_array('0', $storeIds)) {
            array_unshift($storeIds, 0);
        }
        $where = array();
        foreach ($storeIds as $storeId) {
            $where[] = $this->_getConditionSql('store_id', array('finset' => $storeId));
        }
        $this->_select->where(implode(' OR ', $where));
        return $this;
    }

    public function addBlockKeyFilter($blockKey)
    {
        $this->addFieldToFilter('layout_key', $blockKey);
        return $this;
    }

    public function addEnabledFilter()
    {
        $this->addFieldToFilter('status', Meigee_Layouts_Model_Status::STATUS_ENABLED);
        return $this;
    }
}