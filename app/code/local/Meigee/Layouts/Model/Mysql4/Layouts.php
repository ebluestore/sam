<?php

class Meigee_Layouts_Model_Mysql4_Layouts extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the layouts_id refers to the key field in your database table.
        $this->_init('layouts/layouts', 'layouts_id');
    }
}