<?php

$installer = $this;

$installer->startSetup();

$installer->run("
				
-- DROP TABLE IF EXISTS {$this->getTable('layouts/layouts')};
CREATE TABLE {$this->getTable('layouts/layouts')} (
    `layouts_id` int(11) unsigned NOT NULL auto_increment,  
    `title` varchar(255) NOT NULL default '',
    `image` varchar(255) NOT NULL default '', 
    `image_retina` varchar(255) NOT NULL default '',  
    `link` varchar(255) default NULL,
    `layout_key` varchar(255) default NULL,   
    `content` text NOT NULL,
    `status` smallint(6) NOT NULL default '0', 
    `store_id` varchar(255) NOT NULL default '', 
    PRIMARY KEY  (`layouts_id`)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Meigee Layouts';

");

$installer->endSetup();

?>