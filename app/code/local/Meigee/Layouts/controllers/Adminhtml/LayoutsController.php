<?php

class Meigee_Layouts_Adminhtml_LayoutsController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('layouts/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Manage Layouts'), Mage::helper('adminhtml')->__('Manage Layouts'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('layouts/layouts')->load($id);

		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}

			Mage::register('layouts_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('layouts/items');

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
			
			$this->_addContent($this->getLayout()->createBlock('layouts/adminhtml_layouts_edit'))
				->_addLeft($this->getLayout()->createBlock('layouts/adminhtml_layouts_edit_tabs'));
			
			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('layouts')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody($this->getLayout()->createBlock('layouts/adminhtml_layouts_grid')->toHtml());
    }

    public function saveAction() {
        $model = Mage::getModel('layouts/layouts');
        if ($data = $this->getRequest()->getPost()) {

            if(isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') {
                $imgName = $_FILES['image']['name'];
                $imgName = str_replace(' ', '_',$imgName);

                $path = Mage::getBaseDir('media')."/layouts". DS ."images". DS ;
                $uploader = new Varien_File_Uploader('image');
                $uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $destFile = $path.$imgName;
                $imgName  = $model->getNewFileName($destFile);
                $uploader->save($path,$imgName);
                // We set media as the upload dir
                $uploader->save($path, $_FILES['image']['name'] );
                //this way the name is saved in DB
                $data['image'] = $imgName;
            }

            if(isset($_FILES['image_retina']['name']) && $_FILES['image_retina']['name'] != '') {
                $imgRetinaName = $_FILES['image_retina']['name'];
                $imgRetinaName = str_replace(' ', '_',$imgRetinaName);

                $path = Mage::getBaseDir('media')."/layouts". DS ."retinaimages". DS ;
                $uploader = new Varien_File_Uploader('image_retina');
                $uploader->setAllowedExtensions(array('jpg','JPG','jpeg','gif','GIF','png','PNG'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);

                $destFile = $path.$imgRetinaName;
                $imgRetinaName  = $model->getNewFileName($destFile);
                $uploader->save($path,$imgRetinaName);
                // We set media as the upload dir
                $uploader->save($path, $_FILES['image_retina']['name'] );
                //this way the name is saved in DB
                $data['image_retina'] = $imgRetinaName;
            }            

            if(isset($data['stores'])) {
                if(in_array('0',$data['stores'])){
                    $data['store_id'] = '0';
                } else {
                    $data['store_id'] = implode(",", $data['stores']);
                }
                unset($data['stores']);
            } else {
                $data['store_id'] = '0';
            }

            $model = Mage::getModel('layouts/layouts');
            $model->setData($data)->setId($this->getRequest()->getParam('id'));
            try {
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('layouts')->__('Layout was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('layouts')->__('Unable to find Layout to save'));
        $this->_redirect('*/*/');
    }
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('layouts/layouts');
				 
				$model->setId($this->getRequest()->getParam('id'))->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $layoutsIds = $this->getRequest()->getParam('layouts');
        if(!is_array($layoutsIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($layoutsIds as $layoutsId) {
                    $layouts = Mage::getModel('layouts/layouts')->load($layoutsId);
                    $layouts->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($layoutsIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $layoutsIds = $this->getRequest()->getParam('layouts');
        if(!is_array($layoutsIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($layoutsIds as $layoutsId) {
                    $layouts = Mage::getSingleton('layouts/layouts')
                        ->load($layoutsId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($layoutsIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'layouts.csv';
        $content    = $this->getLayout()->createBlock('layouts/adminhtml_layouts_grid')->getCsv();
        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'layouts.xml';
        $content    = $this->getLayout()->createBlock('layouts/adminhtml_layouts_grid')->getXml();
        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}