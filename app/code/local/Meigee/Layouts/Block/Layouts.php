<?php
class Meigee_Layouts_Block_Layouts extends Mage_Core_Block_Template
{
    public function getLayouts()
    {
        $collection = Mage::getModel('layouts/layouts')->getCollection();
        $collection->addStoreFilter(Mage::app()->getStore(true)->getId());
        $collection->addEnabledFilter();
        return $collection;
    }
}