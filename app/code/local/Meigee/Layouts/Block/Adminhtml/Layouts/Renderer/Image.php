<?php

class Meigee_Layouts_Block_Adminhtml_Layouts_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $value = '';
        $img  = Mage::getBaseUrl('media')."layouts/images/".$row->getData('image');
        if($row->getData('image'))
        	$value = '<img src="'.$img .'" width="150" />';
        else
        	$value = 'No Image';
        return $value;
    }
    
}