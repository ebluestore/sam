<?php

class Meigee_Layouts_Block_Adminhtml_Layouts_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'layouts';
        $this->_controller = 'adminhtml_layouts';
        
        $this->_updateButton('save', 'label', Mage::helper('layouts')->__('Save Layout'));
        $this->_updateButton('delete', 'label', Mage::helper('layouts')->__('Delete Layout'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('layouts_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'layouts_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'layouts_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('layouts_data') && Mage::registry('layouts_data')->getId() ) {
            return Mage::helper('layouts')->__("Edit Layout '%s'", $this->htmlEscape(Mage::registry('layouts_data')->getTitle()));
        } else {
            return Mage::helper('layouts')->__('Add Layout');
        }
    }
}