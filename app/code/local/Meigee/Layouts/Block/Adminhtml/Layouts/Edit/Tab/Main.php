<?php

class Meigee_Layouts_Block_Adminhtml_Layouts_Edit_Tab_Main extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareLayout()
    {
        $return = parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
        return $return;
    }

    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $form->setHtmlIdPrefix('editor_');
        $fieldset = $form->addFieldset('layouts_form', array('legend'=>Mage::helper('layouts')->__('Layouts information')));

        try {
            $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
            $wysiwygConfig->setData(
                Mage::helper('layouts')->recursiveReplace(
                    '/layouts_admin/',
                    '/' . (string)Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName') . '/',
                    $wysiwygConfig->getData()
                )
            );
        } catch (Exception $ex) {
            $wysiwygConfig = null;
        }
        
        $object = Mage::getModel('layouts/layouts')->load( $this->getRequest()->getParam('id') );
        
        $imgPath = Mage::getBaseUrl('media')."layouts/images/".$object['image'];
        $imgRetinaPath = Mage::getBaseUrl('media')."layouts/retinaimages/".$object['image_retina'];

        $fieldset->addField('title', 'text', array(
            'label'     => Mage::helper('layouts')->__('Title'),
            'class'     => 'required-entry',
            'required'  => true,
            'name'      => 'title',
        ));

        if($object['image']){
            $fieldset->addField('img', 'label', array(
                'label' => Mage::helper('layouts')->__('Layout Image'),
                'name'  =>'img',
                'value'     => $imgPath,
                'after_element_html' => '<img src="'.$imgPath .'" alt="'. $imgPath .'" height="120" />',
            ));
        }

        if($object['image']){
            $fieldset->addField('image', 'file', array(
                'label'     => Mage::helper('layouts')->__('Change Image'),
                'required'  => false,
                'name'      => 'image',
            ));
        }else {
            $fieldset->addField('image', 'file', array(
                'label'     => Mage::helper('layouts')->__('Choose Image'),
                'required'  => false,
                'name'      => 'image',
            ));
        }

        if($object['image_retina']){
            $fieldset->addField('img_retina', 'label', array(
                'label' => Mage::helper('layouts')->__('Layout Retina Image'),
                'name'  =>'img_retina',
                'value'     => $imgRetinaPath,
                'after_element_html' => '<img src="'.$imgRetinaPath .'" alt="'. $imgRetinaPath .'" height="120" />',
            ));
        }

        if($object['image_retina']){
            $fieldset->addField('image_retina', 'file', array(
                'label'     => Mage::helper('layouts')->__('Change Retina Image'),
                'required'  => false,
                'name'      => 'image_retina',
            ));
        }else {
            $fieldset->addField('image_retina', 'file', array(
                'label'     => Mage::helper('layouts')->__('Choose Retina Image'),
                'required'  => false,
                'name'      => 'image_retina',
            ));
        }

        $fieldset->addField('link', 'text', array(
            'label'     => Mage::helper('layouts')->__('Link'),
            'required'  => false,
            'name'      => 'link',
            'class'     => 'validate-clean-url',
        ));

        $fieldset->addField('layout_key', 'select', array(
            'label'     => Mage::helper('layouts')->__('Block key'),
            'required'  => true,
            'name'      => 'layout_key',
            'class'     => 'required-entry',
            'values'    => Mage::getModel('layouts/key')->toOptionArray(),
            'after_element_html' => Mage::getModel('layouts/key')->getBlockHtml(),
        ));


        if (!Mage::app()->isSingleStoreMode()) {
            $fieldset->addField('store_id', 'multiselect', array(
                'name'      => 'stores[]',
                'label'     => Mage::helper('layouts')->__('Store View'),
                'title'     => Mage::helper('layouts')->__('Store View'),
                'required'  => true,
                'values'    => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, true),
            ));
        } else {
            $fieldset->addField('store_id', 'hidden', array(
                'name' => 'stores[]',
                'value' => Mage::app()->getStore(true)->getId()
            ));
        }

        $fieldset->addField('status', 'select', array(
            'label'     => Mage::helper('layouts')->__('Status'),
            'name'      => 'status',
            'values'    => Mage::getModel('layouts/status')->toOptionArray()
        ));

        $fieldset->addField('content', 'editor', array(
            'name'      => 'content',
            'label'     => Mage::helper('layouts')->__('Content'),
            'title'     => Mage::helper('layouts')->__('Content'),
            'style'     => 'width:800px; height:350px;',
            'config'    => $wysiwygConfig,
            'wysiwyg'   => true,
            'required'  => false,
        ));

        if ( Mage::getSingleton('adminhtml/session')->getLayoutsData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getLayoutsData());
            Mage::getSingleton('adminhtml/session')->setLayoutsData(null);
        } elseif ( Mage::registry('layouts_data') ) {
            $form->setValues(Mage::registry('layouts_data')->getData());
        }
        return parent::_prepareForm();
    }
}