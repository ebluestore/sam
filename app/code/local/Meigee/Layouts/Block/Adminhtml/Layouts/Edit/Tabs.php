<?php

class Meigee_Layouts_Block_Adminhtml_Layouts_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('layouts_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('layouts')->__('Block Information'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('layouts')->__('Block Information'),
            'title'     => Mage::helper('layouts')->__('Block Information'),
            'content'   => $this->getLayout()->createBlock('layouts/adminhtml_layouts_edit_tab_main')->toHtml(),
        ));  
        return parent::_beforeToHtml();
    }
}