<?php
class Meigee_Layouts_Block_Adminhtml_Layouts extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  	public function __construct()
  	{
	    $this->_controller = 'adminhtml_layouts';
	    $this->_blockGroup = 'layouts';
	    $this->_headerText = Mage::helper('layouts')->__('Manage Layouts');
	    $this->_addButtonLabel = Mage::helper('layouts')->__('Add New Block');
	    parent::__construct();
  	}
}