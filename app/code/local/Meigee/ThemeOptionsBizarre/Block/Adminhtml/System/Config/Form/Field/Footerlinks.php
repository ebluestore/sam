<?php
class Meigee_ThemeOptionsBizarre_Block_Adminhtml_System_Config_Form_Field_Footerlinks extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct()
    {
        $this->addColumn('label', array(
            'label' => Mage::helper('ThemeOptionsBizarre')->__('Label'),
            'style' => 'width:150px',
        ));
        $this->addColumn('link', array(
            'label' => Mage::helper('ThemeOptionsBizarre')->__('Link'),
            'style' => 'width:250px',
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('ThemeOptionsBizarre')->__('Add Link');
        parent::__construct();
    }
}