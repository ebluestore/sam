<?php

require_once 'abstract.php';
 
/**
* Magento Attribute Import Shell Script
*
* @category    Mage
* @package     Mage_Shell
*/
class Mage_Shell_Updateprice extends Mage_Shell_Abstract
{
    public function run()
    {
        if ($this->getArg('attribute') && $this->getArg('file')) {
            try{
                $file = $this->getArg('file');
                $csv = new Varien_File_Csv();
                $data = $csv->getData($file);
                $productModel = Mage::getModel('catalog/product');
                
                for($i=1; $i<count($data); $i++){
                    $product = $productModel->getCollection()->addAttributeToSelect('special_price')->addAttributeToFilter('sku', $data[$i][0])->getFirstItem();                    
                    $product->setData($this->getArg('attribute'), NULL)->getResource()->saveAttribute($product, $this->getArg('attribute'));
                }
            }catch(Exception $e){
                echo "Import Error::".$e->getMessage()."n";
            }
        } else {
            echo $this->usageHelp();
        }
    }
 
    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f remove_saleprice.php -- [options]
        php -f remove_saleprice.php -- --attribute <attribute> --file ../var/import/assignattribute.csv
 
  --attribute <attribute>       name of the attribute to update
  --file <file>                 file path to import from, one value per line
  help                          This help
USAGE;
    }
}
 
$shell = new Mage_Shell_Updateprice();
$shell->run();