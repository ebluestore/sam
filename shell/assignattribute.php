<?php

require_once 'abstract.php';
 
/**
* Magento Attribute Import Shell Script
*
* @category    Mage
* @package     Mage_Shell
*/
class Mage_Shell_Assignattribute extends Mage_Shell_Abstract
{
    public function run()
    {
        if ($this->getArg('attribute') && $this->getArg('file')) {
            try{
                $file = $this->getArg('file');
                $csv = new Varien_File_Csv();
                $data = $csv->getData($file);

                $productModel = Mage::getModel('catalog/product');
                $attr = $productModel->getResource()->getAttribute($this->getArg('attribute'));
                
                for($i=1; $i<count($data); $i++){                    
                    if ($attr->usesSource()) {
                        $opt_id = $attr->getSource()->getOptionId($data[$i][1]);
                    }
                    $product = $productModel->getCollection()->addAttributeToFilter('sku', $data[$i][0])->getFirstItem();
                    $product->setData($this->getArg('attribute'), $opt_id)->getResource()->saveAttribute($product, $this->getArg('attribute'));
                }
            }catch(Exception $e){
                echo "Import Error::".$e->getMessage()."n";
            }
        } else {
            echo $this->usageHelp();
        }
    }
 
    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f assignattribute.php -- [options]
        php -f assignattribute.php -- --attribute <attribute> --file ../var/import/assignattribute.csv
 
  --attribute <attribute>       name of the attribute to update
  --file <file>                 file path to import from, one value per line
  help                          This help
USAGE;
    }
}
 
$shell = new Mage_Shell_Assignattribute();
$shell->run();